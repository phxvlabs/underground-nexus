package main

import (
	"dagger.io/dagger"
	"dagger.io/dagger/core"
	"universe.dagger.io/docker"
	// "universe.dagger.io/bash"
	"universe.dagger.io/docker/cli"
)

#DINDBuild: docker.#Dockerfile & {
	dockerfile: contents: """
		    FROM --platform=amd64 docker:dind
			RUN apk update && apk upgrade
			RUN apk add bash nano curl wget docker-compose
		"""
}

#DINDOfficialBuild: docker.#Dockerfile & {
	dockerfile: contents: """
				FROM --platform=amd64 docker:dind
				LABEL authors="Antonette Caldwell <antonette@underground-ops.dev>"
				LABEL description="Custom Underground Nexus copy-paste script data center deployment."
				VOLUME ["/var/run", "/var/lib/docker/volumes", "/nexus-bucket"]
				WORKDIR /nexus-bucket
				COPY ./nexus-bucket /nexus-bucket/
				EXPOSE 22 53 80 443 1000 2375 2376 2377 9010 9443 18443
		"""
}

// Build a base Docker-in-Docker for reuse
dagger.#Plan & {
	client: network: "unix:///var/run/docker.sock": connect: dagger.#Socket
	client: filesystem: ".": read: contents: dagger.#FS
	client: env: {
		AZURE_CONTAINER_REGISTRY: string
		AZURE_CONTAINER_USER:     string
		AZURE_CONTAINER_PASS:     dagger.#Secret
		REGISTRY_USER:            string
		REGISTRY_PASS:            dagger.#Secret
		DOCKERIO_USER:            string
		DOCKERIO_PASS:            dagger.#Secret
	}
	// Build a base test image for testing later, load onto local registry
	actions: testImage: {
		build: #DINDBuild & {
			source: client.filesystem.".".read.contents
		}
		load: cli.#Load & {
			image: build.output
			host:  client.network."unix:///var/run/docker.sock".connect
			tag:   "localhost:5000/underground-nexus-base:amd64-latest"
		}
	}
	// Build next image using existing local image
	actions: buildTestImage: {
		copyFile: core.#Source & {
			path: "./nexus-bucket"
		}

		build: docker.#Build & {
			steps: [
				docker.#Pull & {
					source:      "localhost:5000/underground-nexus-base:amd64-latest"
					resolveMode: "preferLocal"
					auth: {
						username: client.env.REGISTRY_USER
						secret:   client.env.REGISTRY_PASS
					}
				},
				docker.#Set & {
					config: workdir: "/nexus-workbench"
				},
				docker.#Copy & {
					contents: copyFile.output
				},
				docker.#Run & {
					command: {
						name: "apk"
						args: ["add", "bash", "nano", "wget", "curl"]
					}
				},
				docker.#Set & {
					config: {
						expose: {
							"22/tcp": {}
							"53/tcp": {}
							"80/tcp": {}
							"443/tcp": {}
							"1000/tcp": {}
							"2375/tcp": {}
							"2376/tcp": {}
							"2377/tcp": {}
							"9010/tcp": {}
							"9443/tcp": {}
							"18443/tcp": {}
						}
					}
				},
			]
		}
		push: docker.#Push & {
			image: build.output
			auth: {
				username: client.env.AZURE_CONTAINER_USER
				secret:   client.env.AZURE_CONTAINER_PASS
			}
			dest: "\(client.env.AZURE_CONTAINER_REGISTRY)/underground-nexus-test:amd64-latest"
		}
	}
	// Pull Azure Container Registry image and retagged and pushed to Dockerhub as official image
	actions: push: {
		copyFile: core.#Source & {
			path: "./nexus-bucket"
		}

		build: docker.#Build & {
			steps: [
				docker.#Pull & {
					source: "\(client.env.AZURE_CONTAINER_REGISTRY)/underground-nexus-test:amd64-latest"
					auth: {
						username: client.env.AZURE_CONTAINER_USER
						secret:   client.env.AZURE_CONTAINER_PASS
					}
				},
			]
		}
		push: docker.#Push & {
			image: build.output
			auth: {
				username: client.env.DOCKERIO_USER
				secret:   client.env.DOCKERIO_PASS
			}
			dest: "\(client.env.DOCKERIO_USER)/underground-nexus:amd64-latest"
		}
	}
	////
	actions: pushTestImage: {
		build: #DINDBuild & {
			source: client.filesystem.".".read.contents
			auth: "phxvlabsdevacr.azurecr.io": {
				username: client.env.AZURE_CONTAINER_USER
				secret:   client.env.AZURE_CONTAINER_PASS
			}
		}
		push: docker.#Push & {
			image: build.output
			auth: {
				username: client.env.AZURE_CONTAINER_USER
				secret:   client.env.AZURE_CONTAINER_PASS
			}
			dest: "\(client.env.AZURE_CONTAINER_REGISTRY)/underground-nexus-base:amd64-latest"
		}
	}
	//
	actions: testOfficialImage: {
		build: #DINDOfficialBuild & {
			source: client.filesystem.".".read.contents
		}
		load: cli.#Load & {
			image: build.output
			host:  client.network."unix:///var/run/docker.sock".connect
			tag:   "localhost:5000/underground-nexus:amd64-latest"
		}
	}
	//
	actions: pushOfficialImage: {
		build: #DINDOfficialBuild & {
			source: client.filesystem.".".read.contents
			auth: "phxvlabsdevacr.azurecr.io": {
				username: client.env.AZURE_CONTAINER_USER
				secret:   client.env.AZURE_CONTAINER_PASS
			}
		}
		push: docker.#Push & {
			image: build.output
			auth: {
				username: client.env.AZURE_CONTAINER_USER
				secret:   client.env.AZURE_CONTAINER_PASS
			}
			dest: "\(client.env.AZURE_CONTAINER_REGISTRY)/underground-nexus:amd64-latest"
		}
	}
}
