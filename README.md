## Underground Nexus Deployment
> Current v0.8.0

To pull the latest Underground Nexus image.

`docker pull docker.io/phxvlabs/underground-nexus:amd64-latest`

To run with just Docker in `--privileged` mode.

```bash
docker run -itd --name=Underground-Nexus \
    -h Underground-Nexus \
    --privileged \
    --init \
    -p 1000:1000 -p 9050:9443 \
    -v underground-nexus-docker-socket:/var/run \
    -v underground-nexus-data:/var/lib/docker/volumes \
    -v nexus-bucket:/nexus-bucket docker.io/phxvlabs/underground-nexus:amd64-latest
```

To execute the script

`docker exec Underground-Nexus sh deploy-olympiad.sh`

To remove and prune images/volumes

`docker stop Underground-Nexus && docker rm Underground-Nexus && docker volume prune`

**Resources**

> Please note the provided IP address may not be the same as yours.

1. PI-Hole DNS that sits behind NGINX Reverse Proxy - Obtain the IP address of your running docker container. `docker inspect Underground-Nexus`
  - There is no password set, so you will have to change it
    - Access the Docker container once you're within the Underground-Nexus running container.
      - `docker exec -it Inner-DNS-Control /bin/sh`
      - `sudo pihole -a -p`
2. Linux webtop desktop - XFCE or Alpine KDE - `http://10.20.0.30:3000` (optionally, only accessible from the Ubuntu MATE desktop)
3. Linux webtop desktop - Ubuntu MATE - `http://172.17.0.4:1000`
4. Kali Linux Bleeding Edge Repository - Not publicly accessible
  - To access Kali Linux
    - `docker exec -it Underground-Nexus /bin/sh`
    - `docker exec -it Athena0 /bin/bash`
5. Portainer CE - `https://172.17.0.4:9443/`
6. Minio S3 Storage - `http://172.17.0.4:9001/login`
  - To access Minio S3, this server is deployed with the default credentials: `minioadmin:minioadmin`.
7. Visual Studio Code - `http://10.20.0.7:3000` is not publicly accessible, this can be accessed from Ubuntu MATE desktop.
8. Kubernetes (k3d) cluster deployed on standby as a load-balancer.

**Features that are missing:**

UPDATE: 
  - Added Visual Studio Code Server, there were issues with hardware acceleration when it is deployed via ARM64 distro.
  - Minio has been added back again, and the port has been fixed.
  - ~~There is a bash script now, and I took away the workbench.sh script for now.~~ Scratch that, it's added back in the Dockerfile.
  - Docker Swarm is initialized last at the end of the script to avoid conflicts with existing services.

1. ~~Minio is not being used right now because Minio has a conflicting port 9000 with Portainer. Future plan to redirect port usage or take away port 9000 on Portainer.~~
2. There isn't the best bash script to check for logic on when there are certain resources that are already available. A cli tool is coming soon.
3. Docker Swarm is not initialized first only because of the conflict that Portainer has while being deployed along with Docker Swarm turned on.

**ROADMAP**

1. Before, I generated `cosign` key and `cosign` public key, ~~now it has been changed to use GitHub support. There is no local private key to verify.~~ I generated a private key with a local Vault development server. This is only temporary until I switch to a permanent solution where a key can be used efficiently. ~~At this time, signing and verification can be done with GitHub OIDC login, which is the feature `COSIGN_EXPERIMENTAL=1` is being used.~~
2. Future plan is to build an ARM64 version of Underground Nexus.
3. Dagger plan is using `docker#Push`, looking for a better way to implement this steps through Github Actions. (optional)
4. So far, docker images are being built and pushed to DockerHub and Azure Container Registry (testing purpose). Future plan is create an in-house self-hosted registry.

**Information**

UPDATE:
    - I am integrating Dagger into the project where I can build the Docker image with a bit more control. This information below is available just yet.

The default Dockerfile has the original `docker:dind` base image to build the Underground Nexus from. The GitHub Action will build the main Dockerfile with `dagger`. Please note, there are two Dockerfiles. One is using `nestybox/alpine-supervisord-docker:latest` which is an alternate version of Docker-in-Docker (dind). Before, I had multiple steps to build and load the newly built Docker images. I was able to change the Dagger actions to build with the Dockerfile contents.

**Details**

I am using Sigstore Cosign to generate key-pair and sign my docker images with the generated Cosign private key.

Cosign public key

```bash
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAErAQrHGhE+fhmFFz0LPJ59cAgHeDf
Jk0B/vVqGC3XtBpnXUE8TaoVTP95VwLXIs5t6sPBJ4ywy7XzDfruWC/p4g==
-----END PUBLIC KEY-----
```

To verify the signed image with the about Cosign public key. You can also download `crane` to display the signed digest. It is also verifiable on my Dockerhub page.

```bash
docker pull docker.io/phxvlabs/underground-nexus:amd64-latest

cosign triangulate docker.io/phxvlabs/underground-nexus:amd64-latest
index.docker.io/phxvlabs/underground-nexus:sha256-692cf6db82a028bfb7b0f4ec2a26c3403f378fd3fb9a96f168eb2222fee4d251.sig

crane manifest $(cosign triangulate phxvlabs/underground-nexus:amd64-latest) | jq .
{
  "schemaVersion": 2,
  "mediaType": "application/vnd.oci.image.manifest.v1+json",
  "config": {
    "mediaType": "application/vnd.oci.image.config.v1+json",
    "size": 233,
    "digest": "sha256:d2c4a3624b56d79d26451a01e9c74ce927b25132b3181d2615547f77de56bb8e"
  },
  "layers": [
    {
      "mediaType": "application/vnd.dev.cosign.simplesigning.v1+json",
      "size": 353,
      "digest": "sha256:90a340d9b04b6af18d6931e23bb41c869c3ef3d45f2a0f1f51a491b8eb1a7376",
      "annotations": {
        "dev.cosignproject.cosign/signature": "MEUCIGyZG32wou+BDov053vtgLnPAzH1deDU+qmok0YXowgbAiEAuQhdRW9kCS2UuNqOwJBlk/SCWR0m6jeVn8b6dEpGHO8="
      }
    }
  ]
}

cosign verify --key cosign.pub docker.io/phxvlabs/underground-nexus:amd64-latest | jq .

Verification for index.docker.io/phxvlabs/underground-nexus:amd64-latest --
The following checks were performed on each of these signatures:
  - The cosign claims were validated
  - The signatures were verified against the specified public key
[
  {
    "critical": {
      "identity": {
        "docker-reference": "index.docker.io/phxvlabs/underground-nexus"
      },
      "image": {
        "docker-manifest-digest": "sha256:692cf6db82a028bfb7b0f4ec2a26c3403f378fd3fb9a96f168eb2222fee4d251"
      },
      "type": "cosign container image signature"
    },
    "optional": {
      "commit": "b00e7afd7bb3b4d95b9300fefbd030076b120a59",
      "repo": "docker.io/phxvlabs/underground-nexus"
    }
  }
]
```

**Experimental**

> DO NOT ATTEMPT

Since I built this particular Docker image with `sysbox`, you can use `sysbox` as the runtime. To run with `sysbox`, make sure that `sysbox` is installed.

```bash
docker run --runtime=sysbox-runc \
    -itd \
    --name=Underground-Nexus \
    --hostname=Underground-Nexus \
    --mount source=underground-nexus-data,target=/var/lib/docker/volumes \
    --mount source=underground-nexus-docker-socket,target=/var/run \
    --mount source=nexus-bucket,target=/nexus-bucket \
    -p 1000:1000 -p 9050:9443 nexus0:v0.7.1
```