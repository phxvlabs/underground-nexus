#!/bin/sh

set -ex

count=0

echo -n "checking to see workbench is up"
until [ $(docker ps | grep workbench | grep -c -e "Up" -e "running") == 1 ];
do
    if [ $count -eq 12 ]; then
        echo "! Timeout reached"
        exit 1
    else
        echo -n "."
        sleep 5
        let 'count+=1'
    fi
done

docker exec workbench \
    sudo sh -c 'sudo rm /usr/share/backgrounds/ubuntu-mate-common/Green-Wall-Logo.png && \
        sudo wget https://raw.githubusercontent.com/Underground-Ops/underground-nexus/main/Wallpapers/underground-nexus-scifi-space-jelly.png -O /usr/share/backgrounds/ubuntu-mate-common/Green-Wall-Logo.png'