
**Latest Official Image**
image: phxvlabs/underground-nexus:amd64-latest
digest: sha256:692cf6db82a028bfb7b0f4ec2a26c3403f378fd3fb9a96f168eb2222fee4d251
sig: phxvlabs/underground-nexus:sha256-692cf6db82a028bfb7b0f4ec2a26c3403f378fd3fb9a96f168eb2222fee4d251.sig

You can also use `grype` to scan the new docker image.

```bash
grype phxvlabs/underground-nexus:amd64-latest --add-cpes-if-none    
 ✔ Vulnerability DB        [updated]
 ✔ Loaded image            
 ✔ Parsed image            
 ✔ Cataloged packages      [295 packages]
 ✔ Scanned image           [6 vulnerabilities]
NAME                              INSTALLED  FIXED-IN  TYPE       VULNERABILITY        SEVERITY 
github.com/containerd/containerd  v1.6.1     1.6.6     go-module  GHSA-5ffw-gxpp-mxpf  Medium    
github.com/opencontainers/runc    v1.1.0     1.1.2     go-module  GHSA-f3fp-gc8g-vw66  Medium    
google.golang.org/protobuf        v1.27.1              go-module  CVE-2015-5237        High      
google.golang.org/protobuf        v1.27.1              go-module  CVE-2021-22570       High
```

```bash
cosign verify --key cosign.pub phxvlabs/underground-nexus:amd64-latest | jq .

Verification for index.docker.io/phxvlabs/underground-nexus:amd64-latest --
The following checks were performed on each of these signatures:
  - The cosign claims were validated
  - The signatures were verified against the specified public key
[
  {
    "critical": {
      "identity": {
        "docker-reference": "index.docker.io/phxvlabs/underground-nexus"
      },
      "image": {
        "docker-manifest-digest": "sha256:692cf6db82a028bfb7b0f4ec2a26c3403f378fd3fb9a96f168eb2222fee4d251"
      },
      "type": "cosign container image signature"
    },
    "optional": {
      "commit": "b00e7afd7bb3b4d95b9300fefbd030076b120a59",
      "repo": "docker.io/phxvlabs/underground-nexus"
    }
  }
]
```

Each of the image will be signed and have attestation and SBOM attached.

SBOM attached:
image: phxvlabs/underground-nexus
digest: sha256-692cf6db82a028bfb7b0f4ec2a26c3403f378fd3fb9a96f168eb2222fee4d251.sbom
sig: index.docker.io/phxvlabs/underground-nexus:sha256-979330ef8abb7b83586590a12520fd6d3d8c68ba2714e17a42975178d8a5caf7.sig

Attestation attached:
image: phxvlabs/underground-nexus:sha256-692cf6db82a028bfb7b0f4ec2a26c3403f378fd3fb9a96f168eb2222fee4d251.att
---

**Latest Test Image**
image: phxvlabsdevacr.azurecr.io/underground-nexus-test:amd64-latest
digest: sha256:1c7750894370851b8c7adacd3e52aa5d67c176065ec46cb66e27f1e88e47f42b
sig: phxvlabsdevacr.azurecr.io/underground-nexus-test:sha256-1c7750894370851b8c7adacd3e52aa5d67c176065ec46cb66e27f1e88e47f42b.sig

```bash
cosign verify --key cosign.pub phxvlabsdevacr.azurecr.io/underground-nexus-test:amd64-latest | jq .

Verification for phxvlabsdevacr.azurecr.io/underground-nexus-test:amd64-latest --
The following checks were performed on each of these signatures:
  - The cosign claims were validated
  - The signatures were verified against the specified public key
[
  {
    "critical": {
      "identity": {
        "docker-reference": "phxvlabsdevacr.azurecr.io/underground-nexus-test"
      },
      "image": {
        "docker-manifest-digest": "sha256:1c7750894370851b8c7adacd3e52aa5d67c176065ec46cb66e27f1e88e47f42b"
      },
      "type": "cosign container image signature"
    },
    "optional": {
      "commit": "b00e7afd7bb3b4d95b9300fefbd030076b120a59",
      "repo": "phxvlabsdevacr.azurecr.io/underground-nexus-test"
    }
  }
]
```